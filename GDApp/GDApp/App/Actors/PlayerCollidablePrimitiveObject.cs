﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace GDLibrary
{
    public class PlayerCollidablePrimitiveObject : CollidablePrimitiveObject
    {
        #region Fields
        private float moveSpeed, rotationSpeed;
        private EventDispatcher eventDispatcher;
        private Keys[] moveKeys;
        private bool bThirdPersonZoneEventSent;
        private bool onButton = false, onButtonCollided = false;
        float wait = 0;
        private ManagerParameters managerParameters;
        //int currentTime = 0;
        //int elapsed = 0;
        //int savedTime = 100;
        int direction; //0 = pointing north  1 = pointing east
        bool standing = true;
        bool winHit = false;
        int heightChange = 1;
        int smallMoveDistance = 2;
        int largeMoveDistance = 3;
        int rotationAmount = 90;
        Vector3 startingTranslation;



        #endregion

        #region Properties
        public Vector3 StartingTranslation { get => startingTranslation; set => startingTranslation = value; }
        #endregion

        public PlayerCollidablePrimitiveObject(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters,
            StatusType statusType, IVertexData vertexData, ICollisionPrimitive collisionPrimitive,
            ManagerParameters managerParameters,
            Keys[] moveKeys, float moveSpeed, float rotationSpeed, EventDispatcher eventDispatcher)
            : base(id, actorType, transform, effectParameters, statusType, vertexData, collisionPrimitive, managerParameters.ObjectManager)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.rotationSpeed = rotationSpeed;
            
            //for input
            this.managerParameters = managerParameters;
            this.eventDispatcher = eventDispatcher;
            //register with the event dispatcher for the events of interest
            RegisterForEventHandling(eventDispatcher);
        }

        //used to make a player collidable primitives from an existing PrimitiveObject (i.e. the type returned by the PrimitiveFactory
        public PlayerCollidablePrimitiveObject(PrimitiveObject primitiveObject, ICollisionPrimitive collisionPrimitive,
                                ManagerParameters managerParameters, Keys[] moveKeys, float moveSpeed, float rotationSpeed, EventDispatcher eventDispatcher)
            : base(primitiveObject, collisionPrimitive, managerParameters.ObjectManager)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.rotationSpeed = rotationSpeed;

            //for input
            this.managerParameters = managerParameters;
            this.eventDispatcher = eventDispatcher;
            //register with the event dispatcher for the events of interest
            RegisterForEventHandling(eventDispatcher);
        }

        #region Event Handling
        protected virtual void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.PlayerChanged += EventDispatcher_RemoveActorChanged;
        }

        private void EventDispatcher_RemoveActorChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnRemoveActor)
            {

            }
        }
        #endregion


        public override void Update(GameTime gameTime)
        {
            if (wait > 0)
            {
                this.Transform.TranslateIncrement = new Vector3(0, -0.5f, 0);
                //this.Transform.TranslateIncrement = translation;
                //this.Transform.Rotation = rotation;
                wait -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (wait <= 0)
                {
                    if (winHit)
                    {
                        winHit = false;
                        EventDispatcher.Publish(new EventData(EventActionType.OnLevelChange, EventCategoryType.Level));
                        //do event to go to next level
                    }
                    else
                    { 
                    this.Transform.Translation = StartingTranslation + new Vector3(0, 0.5f, 0);
                    this.Transform.Rotation = new Vector3(0, 0, 0);
                        wait = 0;

                    }
                    standing = true;
                    
                }
                
            }
            else
            {
                HandleInput(gameTime);
            }
            //read any input and store suggested increments

            

            //have we collided with something?
            this.Collidee = CheckCollisions(gameTime);

            //how do we respond to this collidee e.g. pickup?
            HandleCollisionResponse(this.Collidee);

            
            //if no collision then move - see how we set this.Collidee to null in HandleCollisionResponse() 
            //below when we hit against a zone
            if (this.Collidee == null)
                ApplyInput(gameTime);

            //reset translate and rotate and update primitive
            base.Update(gameTime);
        }

        //this is where you write the application specific CDCR response for your game
        protected override void HandleCollisionResponse(Actor collidee)
        {
            if (collidee is SimpleZoneObject)
            {
                onButtonCollided = false;
                if (collidee.ID.Equals(AppData.OffTheEdgeID))
                {                    
                    wait = 1000f;
                    //setting this to null means that the ApplyInput() method will get called and the player can move through the zone.
                    this.Collidee = null;
                }
                if (collidee.ID.Equals(AppData.FallHorizontalZoneID))
                {
                    
                    //this.Transform.Rotation = new Vector3(0, 0, 0);
                    wait = 1000f;
                    this.Collidee = null;
                }
                if (collidee.ID.Equals(AppData.WinZoneID))
                {
                    winHit = true;
                    //this.Transform.Rotation = new Vector3(0, 0, 0);
                    //wait = 1000f;
                    object[] additionalParameters = { "winSound" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                    this.Collidee = null;
                }
            }
            else if (collidee is CollidablePrimitiveObject)
            {
                if (collidee.ActorType == ActorType.CollidableDecorator)
                {
                    //we dont HAVE to do anything here but lets change its color just to see something happen
                    //(collidee as DrawnActor3D).EffectParameters.DiffuseColor = Color.Yellow;
                    this.Collidee = null;
                }

                //decide what to do with the thing you've collided with
                else if (collidee.ActorType == ActorType.CollidableAmmo)
                {
                    //do stuff...maybe a remove
                    EventDispatcher.Publish(new EventData(collidee, EventActionType.OnRemoveActor, EventCategoryType.SystemRemove));
                }

                //activate some/all of the controllers when we touch the object
                else if (collidee.ActorType == ActorType.CollidableActivatable)
                {
                    //when we touch get a particular controller to start
                    // collidee.SetAllControllers(PlayStatusType.Play, x => x.GetControllerType().Equals(ControllerType.SineColorLerp));

                    //when we touch get a particular controller to start
                    collidee.SetAllControllers(PlayStatusType.Play, x => x.GetControllerType().Equals(ControllerType.PickupDisappear));
                    this.Collidee = null;
                }

                if (collidee.ActorType == ActorType.CollidableButton)
                {
                    if (!onButtonCollided)
                    {
                        onButtonCollided = true;
                        if (!onButton)
                        {
                            EventDispatcher.Publish(new EventData(EventActionType.OnButtonChange, EventCategoryType.Button));
                            onButton = true;
                        }
                        else
                        {
                            EventDispatcher.Publish(new EventData(EventActionType.OnButtonChange, EventCategoryType.Button));
                            onButton = false;
                        }
                    }
                    this.Collidee = null;
                }
            }
            else
            {
                onButtonCollided = false;
            }
           // Console.WriteLine("onButtonColi: {0}", onButtonCollided);
        }

        protected override void HandleInput(GameTime gameTime)
        {
            
            if (this.managerParameters.KeyboardManager.IsFirstKeyPress(this.moveKeys[AppData.IndexMoveForward])) //Forward
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnMoveChange, EventCategoryType.Move));
                if (!standing)
                {
                    if (direction == 0)
                    {
                        this.Transform.TranslateIncrement = new Vector3(0, heightChange, -largeMoveDistance);
                        this.Transform.Rotation = new Vector3(0, 0, 0);
                        standing = true;
                        
                    }
                    else
                    {
                        this.Transform.TranslateIncrement = new Vector3(0, 0, -smallMoveDistance);
                    }
                }
                else
                {
                    //translation = new Vector3(0, -0.001f, -0.003f);
                    //rotation = new Vector3(0.18f, 0, 0);
                    this.Transform.TranslateIncrement = new Vector3(0, -heightChange, -largeMoveDistance);
                    this.Transform.Rotation = new Vector3(rotationAmount, 0, 0);
                    standing = false;
                    direction = 0;
                    //wait = 1000;
                }

            }
            else if (this.managerParameters.KeyboardManager.IsFirstKeyPress(this.moveKeys[AppData.IndexMoveBackward])) //Backward
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnMoveChange, EventCategoryType.Move));
                if (!standing)
                {
                    if (direction == 0)
                    {
                        
                        this.Transform.TranslateIncrement = new Vector3(0, heightChange, largeMoveDistance);
                        this.Transform.Rotation = new Vector3(0, 0, 0);
                        standing = true;
                    }
                    else
                    {
                        this.Transform.TranslateIncrement = new Vector3(0, 0, smallMoveDistance);
                    }
                }
                else
                {
                    this.Transform.TranslateIncrement = new Vector3(0, -heightChange, largeMoveDistance);
                    this.Transform.Rotation = new Vector3(-rotationAmount, 0, 0);
                    standing = false;
                    direction = 0;
                }
            }

            if (this.managerParameters.KeyboardManager.IsFirstKeyPress(this.moveKeys[AppData.IndexRotateLeft])) //Left
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnMoveChange, EventCategoryType.Move));
                if (!standing)
                {
                    if (direction == 0)
                    {
                        this.Transform.TranslateIncrement = new Vector3(-smallMoveDistance, 0, 0);
                    }
                    else
                    {
                        this.Transform.TranslateIncrement = new Vector3(-largeMoveDistance, heightChange, 0);
                        this.Transform.Rotation = new Vector3(0, 0, 0);
                        standing = true;
                    }
                }
                else
                {
                    this.Transform.TranslateIncrement = new Vector3(-largeMoveDistance, -heightChange, 0);
                    this.Transform.Rotation = new Vector3(0, 0, -rotationAmount);
                    standing = false;
                    direction = 1;
                }
            }
            else if (this.managerParameters.KeyboardManager.IsFirstKeyPress(this.moveKeys[AppData.IndexRotateRight])) //Right
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnMoveChange, EventCategoryType.Move));
                if (!standing)
                {
                    if (direction == 0)
                    {
                        this.Transform.TranslateIncrement = new Vector3(smallMoveDistance, 0, 0);
                    }
                    else
                    {
                        this.Transform.TranslateIncrement = new Vector3(largeMoveDistance, heightChange, 0);
                        this.Transform.Rotation = new Vector3(0, 0, 0);
                        standing = true;
                    }
                }
                else
                {
                               
                this.Transform.TranslateIncrement = new Vector3(largeMoveDistance, -heightChange, 0);
                this.Transform.Rotation = new Vector3(0, 0, rotationAmount);
                standing = false;
                direction = 1;
                }
            }


        }
    }
}
