#define DEMO

using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using System;

/*
 * Diffuse color not pickup up when drawing primitive objects
 * TranslationLerpController
 * Is frustum culling working on primitive objects?
 * 
 * 
 * No statustype on controllers
 * mouse object text interleaving with progress controller
 * Z-fighting on ground plane in 3rd person mode
 * Elevation angle on 3rd person view
 * PiP
 * menu transparency
*/

namespace GDApp
{
    public class Main : Game
    {
        #region Statics
        private readonly Color GoogleGreenColor = new Color(152, 234, 224, 225);
        #endregion

        #region Fields
#if DEBUG
        //used to visualize debug info (e.g. FPS) and also to draw collision skins
        private DebugDrawer debugDrawer;
#endif

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private bool onbutton;
        //added property setters in short-hand form for speed
        public ObjectManager objectManager { get; private set; }
        public CameraManager cameraManager { get; private set; }
        public MouseManager mouseManager { get; private set; }
        public KeyboardManager keyboardManager { get; private set; }
        public ScreenManager screenManager { get; private set; }
        public MyAppMenuManager menuManager { get; private set; }
        public UIManager uiManager { get; private set; }
        public GamePadManager gamePadManager { get; private set; }
        public SoundManager soundManager { get; private set; }
        public MySimplePickingManager pickingManager { get; private set; }

        //receives, handles and routes events
        public EventDispatcher eventDispatcher { get; private set; }

        //stores loaded game resources
        private ContentDictionary<Model> modelDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private ContentDictionary<SpriteFont> fontDictionary;

        //stores curves and rails used by cameras, viewport, effect parameters
        private Dictionary<string, Transform3DCurve> curveDictionary;
        private Dictionary<string, RailParameters> railDictionary;
        private Dictionary<string, Viewport> viewPortDictionary;
        private Dictionary<string, EffectParameters> effectDictionary;
        private ContentDictionary<Video> videoDictionary;
        private Dictionary<string, IVertexData> vertexDataDictionary;

        private ManagerParameters managerParameters;
        private MyPrimitiveFactory primitiveFactory;
        private PlayerCollidablePrimitiveObject playerCollidablePrimitiveObject;
        private PrimitiveDebugDrawer collisionSkinDebugDrawer;
        private RailParameters rail;
        private int currentLevel = 0;
        private PrimitiveObject bridge;
        private PrimitiveObject bridge2;
        private SimpleZoneObject dropZone;
        private SimpleZoneObject dropZone2;
        private int changed = 1;
        private UITextObject GameInfo;
        private int moves;
        #endregion

        #region Properties
        #endregion

        #region Constructor
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        #endregion

        #region Initialization
        protected override void Initialize()
        {
            //moved instanciation here to allow menu and ui managers to be moved to InitializeManagers()
            spriteBatch = new SpriteBatch(GraphicsDevice);

            bool isMouseVisible = true;
            Integer2 screenResolution = ScreenUtility.HD720;
            ScreenUtility.ScreenType screenType = ScreenUtility.ScreenType.SingleScreen;
            int numberOfGamePadPlayers = 1;

            //set the title
            Window.Title = "Qvoid";

            //EventDispatcher
            InitializeEventDispatcher();

            //Dictionaries, Media Assets and Non-media Assets
            LoadDictionaries();
            LoadAssets();
            LoadCurvesAndRails();
            LoadViewports(screenResolution);

            //factory to produce primitives
            LoadFactories();

            //Effects
            InitializeEffects();

            //Managers
            InitializeManagers(screenResolution, screenType, isMouseVisible, numberOfGamePadPlayers);

            //Add Menu and UI elements
            AddMenuElements();
            AddUIElements();
            //Load level contents
            LoadGame(); 

            //Add Camera(s)
            InitializeIntroCamera(screenResolution, 1);
            InitializeCameraDemo(screenResolution);
            
            //Publish Start Event(s)
            StartGame();

#if DEBUG
            //InitializeDebugTextInfo();
           //InitializeDebugCollisionSkinInfo();
#endif
            RegisterForEventHandling(eventDispatcher);
            base.Initialize();
        }

        private void InitializeGameInfo()
        {
            string initalText = "";
            Transform2D transform = null;

            this.GameInfo = null;
            Vector2 position = Vector2.Zero;
            Vector2 scale = Vector2.Zero;
            scale = Vector2.One;

            //to center align horizontaly, it is half window width - lenght of text multiplyed by Scale and fontSize divided by 2
            position = new Vector2((GraphicsDevice.Viewport.Width - (initalText.Length * 60)) / 2, (initalText.Length * 60) / 2);        
            transform = new Transform2D(position, 0, scale, Vector2.Zero, new Integer2(1, 1));
            this.GameInfo = new UITextObject("GameInfo",
                    ActorType.UIDynamicText,
                    StatusType.Drawn | StatusType.Update,
                    transform, Color.ForestGreen,//Light Grey
                    SpriteEffects.None,
                    1,
                    initalText,
                    this.fontDictionary["menu"]);
            this.uiManager.Add(this.GameInfo);


        }
        private void InitializeManagers(Integer2 screenResolution, 
            ScreenUtility.ScreenType screenType, bool isMouseVisible, int numberOfGamePadPlayers) //1 - 4
        {
            //add sound manager
            this.soundManager = new SoundManager(this, this.eventDispatcher, StatusType.Update, "Content/Assets/Audio/", "Demo2DSound.xgs", "WaveBank1.xwb", "SoundBank1.xsb");
            Components.Add(this.soundManager);

            this.cameraManager = new CameraManager(this, 1, this.eventDispatcher);
            Components.Add(this.cameraManager);

            //create the object manager - notice that its not a drawablegamecomponent. See ScreeManager::Draw()
            this.objectManager = new ObjectManager(this, this.cameraManager, this.eventDispatcher, 10);
            
            //add keyboard manager
            this.keyboardManager = new KeyboardManager(this);
            Components.Add(this.keyboardManager);

            //create the manager which supports multiple camera viewports
            this.screenManager = new ScreenManager(this, graphics, screenResolution, screenType,
                this.objectManager, this.cameraManager, this.keyboardManager,
                AppData.KeyPauseShowMenu, this.eventDispatcher, StatusType.Off);
            this.screenManager.DrawOrder = 0;
            Components.Add(this.screenManager);
      
            //add mouse manager
            this.mouseManager = new MouseManager(this, isMouseVisible);
            Components.Add(this.mouseManager);

            //add gamepad manager
            if (numberOfGamePadPlayers > 0)
            {
                this.gamePadManager = new GamePadManager(this, numberOfGamePadPlayers);
                Components.Add(this.gamePadManager);
            }

            //menu manager
            this.menuManager = new MyAppMenuManager(this, this.mouseManager, this.keyboardManager, this.cameraManager, spriteBatch, this.eventDispatcher, StatusType.Off);
            //set the main menu to be the active menu scene
            this.menuManager.SetActiveList("mainmenu");
            this.menuManager.DrawOrder = 3;
            Components.Add(this.menuManager);

            //ui (e.g. reticule, inventory, progress)
            this.uiManager = new UIManager(this, this.spriteBatch, this.eventDispatcher, 10, StatusType.Off);
            this.uiManager.DrawOrder = 4;
            Components.Add(this.uiManager);
        
            //this object packages together all managers to give the mouse object the ability to listen for all forms of input from the user, as well as know where camera is etc.
            this.managerParameters = new ManagerParameters(this.objectManager,
                this.cameraManager, this.mouseManager, this.keyboardManager, this.gamePadManager, this.screenManager, this.soundManager);


            //used for simple picking (i.e. non-JigLibX)
            this.pickingManager = new MySimplePickingManager(this, this.eventDispatcher, StatusType.Update, this.managerParameters);
            Components.Add(this.pickingManager);

        }

        private void LoadDictionaries()
        {
            //models
            this.modelDictionary = new ContentDictionary<Model>("model dictionary", this.Content);

            //textures
            this.textureDictionary = new ContentDictionary<Texture2D>("texture dictionary", this.Content);

            //fonts
            this.fontDictionary = new ContentDictionary<SpriteFont>("font dictionary", this.Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            this.curveDictionary = new Dictionary<string, Transform3DCurve>();

            //rails
            this.railDictionary = new Dictionary<string, RailParameters>();

            //viewports - used to store different viewports to be applied to multi-screen layouts
            this.viewPortDictionary = new Dictionary<string, Viewport>();

            //stores default effect parameters
            this.effectDictionary = new Dictionary<string, EffectParameters>();

            //notice we go back to using a content dictionary type since we want to pass strings and have dictionary load content
            this.videoDictionary = new ContentDictionary<Video>("video dictionary", this.Content);

            //used to store IVertexData (i.e. when we want to draw primitive objects, as in I-CA)
            this.vertexDataDictionary = new Dictionary<string, IVertexData>();

        }
        private void LoadAssets()
        {
            #region Textures
            //environment
            this.textureDictionary.Load("Assets/Textures/Props/Crates/platform2");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/platform3");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/win");

            this.textureDictionary.Load("Assets/Textures/Semitransparent/transparentstripes");


            //menu - buttons
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/genericbtn");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/genericbtnaudio");

            //menu - backgrounds
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/mainmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/audiomenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/controlsmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/exitmenuwithtrans");

            //dual texture demo - see Main::InitializeCollidableGround()
            this.textureDictionary.Load("Assets/GDDebug/Textures/checkerboard_greywhite");

            //levels
            this.textureDictionary.Load("Assets/Textures/Level/level1");
            this.textureDictionary.Load("Assets/Textures/Level/level2");
            this.textureDictionary.Load("Assets/Textures/Level/level3");
            this.textureDictionary.Load("Assets/Textures/Level/level4");
            this.textureDictionary.Load("Assets/Textures/Level/level5");
            this.textureDictionary.Load("Assets/Textures/Level/level6");
            this.textureDictionary.Load("Assets/Textures/Level/level7");
            this.textureDictionary.Load("Assets/Textures/Level/level8");

            //added testing
            this.textureDictionary.Load("Assets/Textures/Foliage/Ground/dark");


#if DEBUG
            //demo
            this.textureDictionary.Load("Assets/GDDebug/Textures/ml");
            this.textureDictionary.Load("Assets/GDDebug/Textures/metal");
            //this.textureDictionary.Load("Assets/Audio/soundtrack");
#endif
            #endregion

            #region Fonts
#if DEBUG
            this.fontDictionary.Load("Assets/GDDebug/Fonts/debug");
#endif
            this.fontDictionary.Load("Assets/Fonts/menu");
            this.fontDictionary.Load("Assets/Fonts/mouse");
            #endregion

        }
        private void LoadCurvesAndRails()
        {
            #region Curves
            Transform3DCurve curveA = new Transform3DCurve(CurveLoopType.Linear); //experiment with other CurveLoopTypes
            curveA.Add(new Vector3(-40, 80, 0), new Vector3(1, -2f, 0f), Vector3.UnitY, 0); //start position          
            curveA.Add(new Vector3(60, 25, 0), new Vector3(-1, -0.4f, 0), Vector3.UnitY, 5);
            curveA.Add(new Vector3(6.1f, 19.3f, 24.4f), new Vector3(0.2f, -0.6f, -0.8f), Vector3.UnitY, 10); //end position
            //add to the dictionary
            this.curveDictionary.Add("introCurveCamera", curveA);
            #endregion

            #region Rails
            //create the track to be applied to the non-collidable track camera 1
            RailParameters rail = new RailParameters("rail1 - parallel to x-axis", new Vector3(-80, 10, 40), new Vector3(80, 10, 40));
            this.railDictionary.Add("rail1 - parallel to x-axis", rail);
            #endregion

        }
        private void InitializeIntroCamera(Integer2 screenResolution, int level)
         {
             
             Transform3D transform = null;
             IController controller = null;
             string id = "";
             string viewportDictionaryKey = "full viewport";
 
            //track camera 1
            id = "IntroCurveCamera";
             transform = new Transform3D(new Vector3(0, 0, 20), -Vector3.UnitZ, Vector3.UnitY);
             controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["introCurveCamera"], PlayStatusType.Play);
             InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);
        }
    private void LoadViewports(Integer2 screenResolution)
        {

            //the full screen viewport with optional padding
            int leftPadding = 0, topPadding = 0, rightPadding = 0, bottomPadding = 0;
            Viewport paddedFullViewPort = ScreenUtility.Pad(new Viewport(0, 0, screenResolution.X, (int)(screenResolution.Y)), leftPadding, topPadding, rightPadding, bottomPadding);
            this.viewPortDictionary.Add("full viewport", paddedFullViewPort);
           
        }
        private void LoadFactories()
        {
            this.primitiveFactory = new MyPrimitiveFactory();
        }

#if DEBUG
        private void InitializeDebugTextInfo()
        {
            //add debug info in top left hand corner of the screen
            this.debugDrawer = new DebugDrawer(this, this.managerParameters, spriteBatch,
                this.fontDictionary["debug"], Color.White, new Vector2(5, 5), this.eventDispatcher, StatusType.Off);
            this.debugDrawer.DrawOrder = 1;
            Components.Add(this.debugDrawer);

        }

        //draws the frustum culling spheres, the collision primitive surfaces, and the zone object collision primitive surfaces based on booleans passed
        private void InitializeDebugCollisionSkinInfo()
        {
            int primitiveCount = 0;
            PrimitiveType primitiveType;

            //used to draw spherical collision surfaces
            IVertexData sphereVertexData = new BufferedVertexData<VertexPositionColor>(
                graphics.GraphicsDevice, PrimitiveUtility.GetWireframeSphere(5, out primitiveType, out primitiveCount), primitiveType, primitiveCount);

            this.collisionSkinDebugDrawer = new PrimitiveDebugDrawer(this, this.eventDispatcher, StatusType.Update | StatusType.Drawn,
                this.managerParameters, true, true, true, sphereVertexData);
            collisionSkinDebugDrawer.DrawOrder = 2;
            Components.Add(collisionSkinDebugDrawer);

        }
#endif
        #endregion


       

        
        #region Load Game
        private void LoadGame()
        {
            
            currentLevel++;
            //non-collidable ground
            int worldScale = 250;
            InitializeNonCollidableGround(worldScale);
            InitializeNonCollidableSkyBox(worldScale);
            //collidable and drivable player
            InitializeCollidablePlayer();

            
            
            InitializeLevelLoaderPrimitives(currentLevel);
            object[] additionalEventParamsB = { "camera level " + currentLevel };
            EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));

        }
        #endregion

       


 

        private void InitializeNonCollidableGround(int worldScale)
        {
            //get the effect relevant to this primitive type (i.e. colored, textured, wireframe, lit, unlit)
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitTexturedPrimitivesEffectID] as BasicEffectParameters;

            //get the primitive object from the factory (remember the factory returns a clone)
            PrimitiveObject ground = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);

            //set the texture
            ground.EffectParameters.Texture = this.textureDictionary["dark"];

            //set the transform
            //since the object is 1 unit in height, we move it down to Y-axis == -0.5f so that the top of the surface is at Y == 0
            ground.Transform = new Transform3D(new Vector3(0, ((-1.0f *worldScale) / 2.0f) - 5, 0), new Vector3(worldScale, 1, worldScale));

            //set an ID if we want to access this later
            ground.ID = "non-collidable ground";

            //add 
            this.objectManager.Add(ground);

            //PrimitiveObject wall = ground.GetDeepCopy() as PrimitiveObject;
            //wall.Transform = new Transform3D(new Vector3(0, -20f, worldScale/2), new Vector3(worldScale, 1, worldScale));
            //wall.Transform.Rotation = new Vector3(90, 0, 0);
            //this.objectManager.Add(wall);
        }

        private void InitializeNonCollidableSkyBox(int worldScale)
        {
            // worldScale /= 2;
            //first we will create a prototype plane and then simply clone it for each of the skybox decorator elements (e.g. ground, front, top etc). 
            Transform3D transform = new Transform3D(new Vector3(0, -5, 0), new Vector3(worldScale, 1, worldScale));

            //get the effect relevant to this primitive type (i.e. colored, textured, wireframe, lit, unlit)
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;

            //get the archetype from the factory
            PrimitiveObject Skybox = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);
            PrimitiveObject clonePlane = null;
            //set texture once so all clones have the same
            Skybox.EffectParameters.Texture = this.textureDictionary["dark"];

            #region Skybox

            //
            clonePlane = Skybox.Clone() as PrimitiveObject;

            clonePlane.Transform.Rotation = new Vector3(90, 0, 0);

            clonePlane.Transform.Translation = new Vector3(0, -5, (-1.0f * worldScale) / 2.0f);

            clonePlane.Transform.ScaleBy(new Vector3(worldScale, 1, worldScale));
            this.objectManager.Add(clonePlane);

            //As an exercise the student should add the remaining 4 skybox planes here by repeating the clone, texture assignment, rotation, and translation steps above...
            //add the left skybox plane
            clonePlane = Skybox.Clone() as PrimitiveObject;

            clonePlane.Transform.Rotation = new Vector3(90, 90, 0);
            clonePlane.Transform.Translation = new Vector3((-1.0f * worldScale) / 2.0f, -5, 0);

            clonePlane.Transform.ScaleBy(new Vector3(worldScale, 1, worldScale));

            this.objectManager.Add(clonePlane);

            //add the right skybox plane
            clonePlane = Skybox.Clone() as PrimitiveObject;

            clonePlane.Transform.Rotation = new Vector3(90, -90, 0);
            clonePlane.Transform.Translation = new Vector3((worldScale) / 2.0f, -5, 0);

            clonePlane.Transform.ScaleBy(new Vector3(worldScale, 1, worldScale));
            this.objectManager.Add(clonePlane);

            //add the top skybox plane
            clonePlane = Skybox.Clone() as PrimitiveObject;

            clonePlane.Transform.Rotation = new Vector3(180, -90, 0);
            clonePlane.Transform.Translation = new Vector3(0, ((worldScale) / 2.0f) - 5, 0);
            clonePlane.Transform.ScaleBy(new Vector3(worldScale, 1, worldScale));
            this.objectManager.Add(clonePlane);

            //add the front skybox plane
            clonePlane = Skybox.Clone() as PrimitiveObject;

            clonePlane.Transform.Rotation = new Vector3(-90, 0, 180);
            clonePlane.Transform.Translation = new Vector3(0, -5, (worldScale) / 2.0f);
            clonePlane.Transform.ScaleBy(new Vector3(worldScale, 1, worldScale));
            this.objectManager.Add(clonePlane);
            #endregion
        }

        private void InitializeCollidableButton()
        {
            //get the effect relevant to this primitive type (i.e. colored, textured, wireframe, lit, unlit)
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;

            //get the archetypal primitive object from the factory
            PrimitiveObject archetypeObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCylinder, effectParameters);

            //set the texture that all clones will have
            archetypeObject.EffectParameters.Texture = this.textureDictionary["platform3"];

            Transform3D transform;
            CollidablePrimitiveObject collidablePrimitiveObject;
            // IController controller;


            //remember the primitive is at Transform3D.Zero so we need to say where we want OUR player to start
            transform = new Transform3D(new Vector3(14, 6, 14), Vector3.Zero, new Vector3(0.8f, 0.1f, 0.8f), Vector3.UnitX, Vector3.UnitY);
            if(currentLevel == 8)
            {
                transform = new Transform3D(new Vector3(24, 6, 6), Vector3.Zero, new Vector3(0.8f, 0.1f, 0.8f), Vector3.UnitX, Vector3.UnitY);
            }
            //make the collidable primitive
            collidablePrimitiveObject = new CollidablePrimitiveObject(archetypeObject.Clone() as PrimitiveObject,
                new BoxCollisionPrimitive(transform), this.objectManager);

            //do we want an actor type for CDCR?
            collidablePrimitiveObject.ActorType = ActorType.CollidableButton;

            //set the position otherwise the boxes will all have archetypeObject.Transform positional properties
            collidablePrimitiveObject.Transform = transform;

            this.objectManager.Add(collidablePrimitiveObject);

        }
        private void InitializeCollidablePlayer()
        {
            //get the effect relevant to this primitive type (i.e. colored, textured, wireframe, lit, unlit)
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;
            
            //get the archetypal primitive object from the factory
            PrimitiveObject primitiveObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);
       
            //remember the primitive is at Transform3D.Zero so we need to say where we want OUR player to start
            Transform3D transform = new Transform3D(new Vector3(2, 8, 8), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY);           
            //instanciate a box primitive at player position
            BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform);
            
            //make the player object and store as field for use by the 3rd person camera - see camera initialization
            this.playerCollidablePrimitiveObject = new PlayerCollidablePrimitiveObject(primitiveObject, collisionPrimitive, 
                this.managerParameters, AppData.PlayerOneMoveKeys, AppData.PlayerMoveSpeed, AppData.PlayerRotationSpeed, this.eventDispatcher);
            this.playerCollidablePrimitiveObject.ActorType = ActorType.Player;
            this.playerCollidablePrimitiveObject.Transform = transform;

            //do we want a texture?
            playerCollidablePrimitiveObject.EffectParameters.Texture = this.textureDictionary["metal"];

            //set an ID if we want to access this later
            playerCollidablePrimitiveObject.ID = "collidable player";
            playerCollidablePrimitiveObject.StartingTranslation = transform.Translation;
            //add to the object manager
            this.objectManager.Add(playerCollidablePrimitiveObject);
        }

        private void InitializeCollidableBridgeCombo()
        {
            Transform3D transform = null;
            
            ICollisionPrimitive collisionPrimitive = null;
            transform = new Transform3D(new Vector3(10, 1000, 2), new Vector3(1, 4, 1));
            
            //we can have a sphere or a box - its entirely up to the developer
            //collisionPrimitive = new SphereCollisionPrimitive(transform, 0.1f);
            collisionPrimitive = new BoxCollisionPrimitive(transform);

            dropZone = new SimpleZoneObject(AppData.OffTheEdgeID, ActorType.Zone, transform,
                StatusType.Drawn | StatusType.Update, collisionPrimitive);//, mParams);
            if (currentLevel == 8)
            {
                dropZone.Transform.Translation = new Vector3(20, 5, 20);
            }
            this.objectManager.Add(dropZone);
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;

            //get the archetype from the factory
            bridge = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);
            bridge.Transform.Scale = new Vector3(2, 2, 2);
            //set texture once so all clones have the same
            bridge.EffectParameters.Texture = this.textureDictionary["platform2"];
            bridge.ActorType = ActorType.CollidableDecorator;
            bridge.Transform.Translation = new Vector3(10, 5, 2);

            if(currentLevel == 8)
            {
                bridge.Transform.Translation = new Vector3(20, 1000, 20);
            }
            this.objectManager.Add(bridge);

            if (currentLevel == 7)
            {
                dropZone2 = dropZone.GetDeepCopy() as SimpleZoneObject;
                dropZone2.Transform.Translation = new Vector3(10, 1000, 4);
                this.objectManager.Add(dropZone2);

                bridge2 = bridge.GetDeepCopy() as PrimitiveObject;
                bridge2.Transform.Translation = new Vector3(10, 5, 4);
                this.objectManager.Add(bridge2);
            }
        }

        private void InitializeLevelLoaderPrimitives(int level)
        {
            Transform3D transform = new Transform3D(new Vector3(4, 8, 4), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY); 
            LevelLoader levelLoader = new LevelLoader(this.textureDictionary, "level1");
            if (level == 1)
            {
                levelLoader = new LevelLoader(this.textureDictionary, "level1");
            }
            else if (level == 2)
            {
               levelLoader = new LevelLoader(this.textureDictionary, "level2");
                transform = new Transform3D(new Vector3(4, 8, 8), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY);
            }
            else if (level == 3)
            {
                levelLoader = new LevelLoader(this.textureDictionary, "level3");
                transform = new Transform3D(new Vector3(2, 8, 8), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY);
            }
            else if (level == 4)
            {
               levelLoader = new LevelLoader(this.textureDictionary, "level4");
                transform = new Transform3D(new Vector3(4, 8, 12), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY);
            }
            else if (level == 5)
            {
                transform = new Transform3D(new Vector3(2, 8, 2), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY);
                levelLoader = new LevelLoader(this.textureDictionary, "level5");
            }
            else if (level == 6)
            {
                transform = new Transform3D(new Vector3(26, 8, 8), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY);                
                levelLoader = new LevelLoader(this.textureDictionary, "level6");
            }
            else if (level == 7)
            {
                InitializeCollidableButton();
                InitializeCollidableBridgeCombo();
                transform = new Transform3D(new Vector3(2, 8, 12), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY);
                levelLoader = new LevelLoader(this.textureDictionary, "level7");

            }
            else if (level == 8)
            {
                InitializeCollidableButton();
                InitializeCollidableBridgeCombo();
                transform = new Transform3D(new Vector3(2, 8, 12), Vector3.Zero, new Vector3(2, 4, 2), Vector3.UnitX, Vector3.UnitY);
                levelLoader = new LevelLoader(this.textureDictionary, "level8");
            }
            else
            {
                this.GameInfo.Text = "You Win!\nTotal Moves: " + moves;
            }
            this.playerCollidablePrimitiveObject.Transform = transform;
            this.playerCollidablePrimitiveObject.StartingTranslation = transform.Translation;

            #region platforms
            //get the effect relevant to this primitive type (i.e. colored, textured, wireframe, lit, unlit)
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;
            
            //get the archetype from the factory
            PrimitiveObject crateA = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);
            crateA.Transform.Scale = new Vector3(2, 2, 2);
            //set texture once so all clones have the same
            crateA.EffectParameters.Texture = this.textureDictionary["platform2"];

            //delicate platforms//
            PrimitiveObject crateB = crateA.GetDeepCopy() as PrimitiveObject;
            crateB.EffectParameters.Alpha = 0.4f;
            crateB.EffectParameters.DiffuseColor = Color.Red;
            crateB.EffectParameters.Texture = this.textureDictionary["platform3"];

            PrimitiveObject crateC = crateA.GetDeepCopy() as PrimitiveObject;
            crateC.EffectParameters.Alpha = 0.9f;
            crateC.EffectParameters.Texture = this.textureDictionary["win"];
            #endregion

            #region zones
            SimpleZoneObject boxCollisionAroundArena = null;
            SimpleZoneObject sphereCollisionAboveWeakPlatforms = null;
            ICollisionPrimitive collisionPrimitive = null;
            //place the zone and scale it based on how big you want the zone to be
            transform = new Transform3D(new Vector3(16, 10, 8), new Vector3(1, 5, 1));

            //we can have a sphere or a box - its entirely up to the developer
            //collisionPrimitive = new SphereCollisionPrimitive(transform, 0.1f);
            collisionPrimitive = new BoxCollisionPrimitive(transform);

            boxCollisionAroundArena = new SimpleZoneObject(AppData.OffTheEdgeID, ActorType.Zone, transform,
                StatusType.Drawn | StatusType.Update, collisionPrimitive);//, mParams);


            transform = new Transform3D(new Vector3(16, 10, 8), new Vector3(1, 1, 1));
            collisionPrimitive = new SphereCollisionPrimitive(transform, 0.1f);
            sphereCollisionAboveWeakPlatforms = new SimpleZoneObject(AppData.OffTheEdgeID, ActorType.Zone, transform,
                StatusType.Drawn | StatusType.Update, collisionPrimitive);//, mParams);

            SimpleZoneObject winZone = sphereCollisionAboveWeakPlatforms.GetDeepCopy() as SimpleZoneObject;
            winZone.ID = AppData.WinZoneID;


            #endregion

            //define what each pixel color should generate
            levelLoader.AddColorActor3DPair(Color.Red, crateA);
            levelLoader.AddColorZonePair(Color.Green, boxCollisionAroundArena);
            levelLoader.AddColorActor3DPair(Color.Orange, crateB);
            levelLoader.AddColorZonePair(Color.Orange, sphereCollisionAboveWeakPlatforms);
            levelLoader.AddColorZonePair(Color.Purple, winZone);
            levelLoader.AddColorActor3DPair(Color.Purple, crateC);
            //levelLoader.AddColorActor3DPair(Color.Pink, playerCollidablePrimitiveObject);

            //process the pixels in the level texture and return a list of actors
            List<Actor3D> listOfActors = levelLoader.Process(new Vector2(2, 2), 5, Vector3.Zero);

            //add the actors to the object manager
            this.objectManager.Add(listOfActors);
        }


        
       

        #region Initialize Cameras
        private void InitializeCamera(Integer2 screenResolution, string id, Viewport viewPort, Transform3D transform, IController controller, float drawDepth)
        {
            Camera3D camera = new Camera3D(id, ActorType.Camera, transform, ProjectionParameters.StandardShallowSixteenNine, viewPort, drawDepth, StatusType.Update);

            if (controller != null)
                camera.AttachController(controller);

            this.cameraManager.Add(camera);
        }

        //adds three camera from 3 different perspectives that we can cycle through
        private void InitializeCameraDemo(Integer2 screenResolution)
        {
            #region Flight Camera
            Transform3D transform = new Transform3D(new Vector3(15f, 54f, 13f), new Vector3(0, -1, -0.1f), Vector3.UnitY);

            IController controller = new FlightCameraController("fcc", ControllerType.FirstPerson, AppData.CameraMoveKeys,
                AppData.CameraMoveSpeed, AppData.CameraStrafeSpeed, AppData.CameraRotationSpeed, this.managerParameters);

            InitializeCamera(screenResolution, AppData.FlightCameraID, this.viewPortDictionary["full viewport"], transform, controller, 0);
            #endregion
            for (int i = 1; i <= 8; i++)
            {
                if (i == 0)
                {
                    transform = new Transform3D(new Vector3(8f, 23.4f, 28f), new Vector3(0.2f, -0.6f, -0.8f), Vector3.UnitY);
                    //transform = new Transform3D(new Vector3(15f, 54f, 13f), new Vector3(0, -1, -0.1f), Vector3.UnitY);
                }
                else if (i ==1)
                {
                    transform = new Transform3D(new Vector3(8f, 23.4f, 28f), new Vector3(0.2f, -0.6f, -0.8f), Vector3.UnitY);
                }
                else if (i ==2)
                {
                    transform = new Transform3D(new Vector3(9.7f, 25.3f, 31f), new Vector3(0.2f, -0.7f, -0.7f), Vector3.UnitY);
                }
                else if (i ==3)
                {
                    transform = new Transform3D(new Vector3(10.6f, 25.3f, 31.3f), new Vector3(0.1f, -0.7f, -0.7f), Vector3.UnitY);
                }
                else if (i ==4)
                {
                    transform = new Transform3D(new Vector3(5, 25.7f, 30.1f), new Vector3(0.1f, -0.7f, -0.7f), Vector3.UnitY);
                }
                else if(i == 7)
                {
                    transform = new Transform3D(new Vector3(12.3f, 27.6f, 35.8f), new Vector3(0.1f, -0.7f, -0.7f), Vector3.UnitY);
                }
                else
                {
                    transform = new Transform3D(new Vector3(12.3f, 27.6f, 31.8f), new Vector3(0.1f, -0.7f, -0.7f), Vector3.UnitY);
                }

                controller = new FlightCameraController("fcc" + (i+1), ControllerType.FirstPerson, AppData.CameraMoveKeys,
                    AppData.CameraMoveSpeed, AppData.CameraStrafeSpeed, AppData.CameraRotationSpeed, this.managerParameters);
            InitializeCamera(screenResolution, "camera level " + (i+1), this.viewPortDictionary["full viewport"], transform,controller, 0);
            }
            #region LevelCameras


            #endregion

        }

        #endregion

        #region Events
        private void InitializeEventDispatcher()
        {
            //initialize with an arbitrary size based on the expected number of events per update cycle, increase/reduce where appropriate
            this.eventDispatcher = new EventDispatcher(this, 20);

            //dont forget to add to the Component list otherwise EventDispatcher::Update won't get called and no event processing will occur!
            Components.Add(this.eventDispatcher);
        }

        private void StartGame()
        {
            //will be received by the menu manager and screen manager and set the menu to be shown and game to be paused
            EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.MainMenu));

            //publish an event to set the camera
            object[] additionalEventParamsB = { "IntroCurveCamera" };
            EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
            object[] additionalParameters = { "soundtrack" };
            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
        }
        #endregion

        #region Menu & UI
        private void AddMenuElements()
        {
            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            UIButtonObject uiButtonObject = null, clone = null;
            string sceneID = "", buttonID = "", buttonText = "";
            int verticalBtnSeparation = 50;

            #region Main Menu
            sceneID = "main menu";

            //retrieve the background texture
            texture = this.textureDictionary["mainmenu"];
            //scale the texture to fit the entire screen
            Vector2 scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);

            this.menuManager.Add(sceneID, new UITextureObject("mainmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add start button
            buttonID = "startbtn";
            buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth/ 1.5f, 350);
            texture = this.textureDictionary["genericbtn"];
            transform = new Transform2D(position,
                0, new Vector2(1.8f, 0.6f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, Color.LightPink, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.Black, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);


            //add audio button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();           
            clone.ID = "audiobtn";
            clone.Text = "Audio";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            //change the texture blend color           
            this.menuManager.Add(sceneID, clone);

            //add controls button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "controlsbtn";
            clone.Text = "Instructions";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            //change the texture blend color
            clone.Color = Color.LightBlue;
            this.menuManager.Add(sceneID, clone);

            //add exit button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "exitbtn";
            clone.Text = "Exit";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            //change the texture blend color
            clone.Color = Color.LightYellow;
            //store the original color since if we modify with a controller and need to reset
            clone.OriginalColor = clone.Color;
            //attach another controller on the exit button just to illustrate multi-controller approach
            clone.AttachController(new UIColorSineLerpController("colorSineLerpController", ControllerType.SineColorLerp,
                    new TrigonometricParameters(1, 0.4f, 0), Color.LightSeaGreen, Color.LightGreen));
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Audio Menu
            sceneID = "audio menu";

            //retrieve the audio menu background texture
            texture = this.textureDictionary["audiomenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("audiomenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));


            //add volume up button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.Texture = this.textureDictionary["genericbtnaudio"];
            clone.ID = "volumeUpbtn";
            clone.Text = "Volume Up";
            //change the texture blend color
            this.menuManager.Add(sceneID, clone);

            //add volume down button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.Texture = this.textureDictionary["genericbtnaudio"];
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            clone.ID = "volumeDownbtn";
            clone.Text = "Volume Down";
            //change the texture blend color
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.Texture = this.textureDictionary["genericbtnaudio"];
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            clone.ID = "volumeMutebtn";
            clone.Text = "Volume Mute";
            //change the texture blend color
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.Texture = this.textureDictionary["genericbtnaudio"];
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            clone.ID = "volumeUnMutebtn";
            clone.Text = "Volume Un-mute";
            //change the texture blend color
            this.menuManager.Add(sceneID, clone);

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.Texture = this.textureDictionary["genericbtnaudio"];
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 4 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Controls Menu
            sceneID = "controls menu";

            //retrieve the controls menu background texture
            texture = this.textureDictionary["controlsmenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 9 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            this.menuManager.Add(sceneID, clone);
            #endregion
        }
        private void AddUIElements()
        {
            InitializeGameInfo();
        }
        private void InitializeUIMousePointer()
        {
            Texture2D texture = this.textureDictionary["reticuleDefault"];
            //show complete texture
            Microsoft.Xna.Framework.Rectangle sourceRectangle = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height);

            //listens for object picking events from the object picking manager
            UIPickingMouseObject myUIMouseObject = new UIPickingMouseObject("picking mouseObject",
                ActorType.UITexture,
                new Transform2D(Vector2.One),
                this.fontDictionary["mouse"],
                "",
                new Vector2(0, 40),
                texture,
                this.mouseManager,
                this.eventDispatcher);
            this.uiManager.Add(myUIMouseObject);
        }        

        #region Effects
        private void InitializeEffects()
        {
            BasicEffect basicEffect = null;
           
            #region For unlit colored primitive objects incl. simple lines and wireframe primitives
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = false;
            basicEffect.VertexColorEnabled = true;
            this.effectDictionary.Add(AppData.UnLitColoredPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For unlit textured primitive objects
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = true;
            basicEffect.VertexColorEnabled = false;
            this.effectDictionary.Add(AppData.UnLitTexturedPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For lit (i.e. normals defined) textured primitive objects
            basicEffect = new BasicEffect(graphics.GraphicsDevice);  
            basicEffect.TextureEnabled = true;
            basicEffect.VertexColorEnabled = false;
            basicEffect.EnableDefaultLighting();
            basicEffect.PreferPerPixelLighting = true;
            this.effectDictionary.Add(AppData.LitTexturedPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion

        }
        #endregion
        #endregion
        #region Content, Update, Draw        


        protected override void Update(GameTime gameTime)
        {
            //exit using new gamepad manager
            if(this.gamePadManager != null && this.gamePadManager.IsPlayerConnected(PlayerIndex.One) && this.gamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.Back))
                this.Exit();

#if DEMO
            DoDebugToggleDemo();
            DoLevelCameraSwitch();
#endif
            DoLevelCameraSwitch();
            DoOverheadSwitch();
            base.Update(gameTime);
        }

        private void DoLevelCameraSwitch()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.C))
            {
                object[] additionalEventParamsB = { "camera level" + currentLevel};
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
            }
        }

        private void DoOverheadSwitch()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.V))
            {
                object[] additionalEventParamsB = { AppData.FlightCameraID };
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
            }
        }

        private void DoDebugToggleDemo()
        {
            if(this.keyboardManager.IsFirstKeyPress(Keys.F5))
            {
                //toggle the boolean variables directly in the debug drawe to show CDCR surfaces
                this.collisionSkinDebugDrawer.ShowCollisionSkins = !this.collisionSkinDebugDrawer.ShowCollisionSkins;
                this.collisionSkinDebugDrawer.ShowZones = !this.collisionSkinDebugDrawer.ShowZones;
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.F6))
            {
                //toggle the boolean variables directly in the debug drawer to show the frustum culling bounding spheres
                this.collisionSkinDebugDrawer.ShowFrustumCullingSphere = !this.collisionSkinDebugDrawer.ShowFrustumCullingSphere;
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.F7))
            {
                //we can turn all debug off 
                EventDispatcher.Publish(new EventData(EventActionType.OnToggleDebug, EventCategoryType.Debug));
            }
            if (this.keyboardManager.IsFirstKeyPress(Keys.L))
            {
                //EventDispatcher.Publish(new EventData(EventActionType.OnRemoveActor, EventCategoryType.Player));
                EventDispatcher.Publish(new EventData(EventActionType.OnLevelChange, EventCategoryType.Level));
            }

        }
        #region Event Handling
        protected virtual void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.LevelChanged += EventDispatcher_LevelChanged;
            eventDispatcher.ButtonChanged += EventDispatcher_ButtonChanged;
            eventDispatcher.MoveChanged += EventDispatcher_MoveChanged;
        }

        private void EventDispatcher_LevelChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnLevelChange)
            {
                this.objectManager.OpaqueDrawList.Clear();
                this.objectManager.TransparentDrawList.Clear();
                this.GameInfo.Text = "Moves: " + moves;
                LoadGame();
            }
        }
        private void EventDispatcher_MoveChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnMoveChange)
            {
                moves++;
                GameInfo.Text = "Moves: " + moves;
            }
        }

        private void EventDispatcher_ButtonChanged(EventData eventData)
        {

            if (eventData.EventType == EventActionType.OnButtonChange)
            {
                changed *= -1;
                if(changed == -1)
                {
                    if (currentLevel == 8)
                    {
                        bridge.Transform.Translation = new Vector3(20, 5, 20);
                        dropZone.Transform.Translation = new Vector3(8, 1000, 8);
                    }
                    else
                    {
                        bridge.Transform.Translation = new Vector3(8, 1000, 8);
                        dropZone.Transform.Translation = new Vector3(10, 10, 2);
                    }
                    
                    
                    if (currentLevel ==7)
                    {
                        dropZone2.Transform.Translation = new Vector3(10, 10, 4);
                                          
                        bridge2.Transform.Translation = new Vector3(10, 1000, 4);
                    }
                }
                else
                {
                    if(currentLevel == 8)
                    {
                        bridge.Transform.Translation = new Vector3(20, 1000, 20);
                        dropZone.Transform.Translation = new Vector3(20, 10, 20);
                        
                    }
                    else
                    {
                        bridge.Transform.Translation = new Vector3(10, 5, 2);
                        dropZone.Transform.Translation = new Vector3(8, 1000, 8);
                    }
                    

                    if (currentLevel == 7)
                    {
                        dropZone2.Transform.Translation = new Vector3(10, 1000, 4);
                        bridge2.Transform.Translation = new Vector3(10, 5, 4);
                    }
                }
                                  
            }



        }
        #endregion
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(GoogleGreenColor);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
        #endregion
    }
}

